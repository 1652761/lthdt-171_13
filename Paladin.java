
public class Paladin extends Knight {

	public Paladin(int baseHp, int wp) {
		super(baseHp, wp);
	}
	public double getCombatScore() {
		int a =1 , b=1 , fibo=0;
		for(int i=3; fibo<getBaseHp(); i++){
			fibo = a+b;
			a=b; b=fibo;
			if(getBaseHp()==fibo) return 1000+i;
		} 
		return getBaseHp()*3;
	}
	
}
